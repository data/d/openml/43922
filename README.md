# OpenML dataset: mushroom

https://www.openml.org/d/43922

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Mushroom records drawn from The Audubon Society Field Guide to North American Mushrooms (1981). G. H. Lincoff (Pres.), New York: Alfred A. Knopf

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43922) of an [OpenML dataset](https://www.openml.org/d/43922). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43922/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43922/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43922/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

